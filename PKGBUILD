# Maintainer: Antoni Aloy Torrens <antoni.aloytorrens@gmail.com>
pkgname=python3.12
pkgver=3.12.7
pkgrel=4
pkgdesc="Python Interpreter"
arch=('amd64')
LICENSE=('PSF-2.0')
bionic_makedepends=('wget' 'build-essential' 'libncursesw5-dev' 'libssl-dev' 'libsqlite3-dev' 'libgdbm-dev' 'libc6-dev' 'libbz2-dev' 'zlib1g-dev' 'liblzma-dev' 'libffi-dev' 'uuid-dev' 'libexpat1-dev')
bionic_depends=('wget' 'libncursesw5' 'libssl1.1' 'sqlite3' 'libgdbm5' 'libc6' 'bzip2' 'zlib1g' 'libffi6' 'libexpat1')
buster_makedepends=('wget' 'build-essential' 'libncursesw5-dev' 'libssl-dev' 'libsqlite3-dev' 'libgdbm-dev' 'libc6-dev' 'libbz2-dev' 'zlib1g-dev' 'liblzma-dev' 'libffi-dev' 'uuid-dev' 'libexpat1-dev')
buster_depends=('wget' 'libncursesw5' 'libssl1.1' 'sqlite3' 'libgdbm6' 'libc6' 'bzip2' 'zlib1g' 'libffi6' 'libexpat1')
bullseye_makedepends=('wget' 'build-essential' 'libncursesw5-dev' 'libssl-dev' 'libsqlite3-dev' 'libgdbm-dev' 'libc6-dev' 'libbz2-dev' 'zlib1g-dev' 'liblzma-dev' 'libffi-dev' 'uuid-dev' 'libexpat1-dev')
bullseye_depends=('wget' 'libncursesw5' 'libssl1.1' 'sqlite3' 'libgdbm6' 'libc6' 'bzip2' 'zlib1g' 'libffi7' 'libexpat1')
url="https://www.python.org"
source=("https://www.python.org/ftp/python/${pkgver}/Python-${pkgver}.tgz")
sha256sums=('73ac8fe780227bf371add8373c3079f42a0dc62deff8d612cd15a618082ab623')

# The necessary bits to build these optional modules were not found:
# _curses
# _curses_panel
# _dbm
# _tkinter
# readline

build() {
        # Necessary variables (otherwise arm architectures will complain about `C compiler cannot create executables`)
        export DEB_BUILD_GNU_TYPE=$(cut -d "=" -f2 <<< $(dpkg-architecture | grep DEB_BUILD_GNU_TYPE))
        export DEB_HOST_GNU_TYPE=$(cut -d "=" -f2 <<< $(dpkg-architecture | grep DEB_HOST_GNU_TYPE))
        export DEB_HOST_MULTIARCH=$(cut -d "=" -f2 <<< $(dpkg-architecture | grep DEB_HOST_MULTIARCH))

        # C compiler will not create executables due to -march and -mtune is detected as x86_64 (as we are running in chroot)
        # Override this and remove march and mtune.
        # Also, in ARM architectures: cc1: error: '-fcf-protection=full' is not supported for this target (see config.log file)
        # We need to remove this CFLAG.

        # For other arch, compile with this
        export CFLAGS='-O2 -pipe -fno-plt -fexceptions -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security -fstack-clash-protection -fcf-protection'
        

        if [[ $DEB_HOST_GNU_TYPE == *"aarch64"* ]] || [[ $DEB_HOST_GNU_TYPE == *"armhf"* ]] || [[ $DEB_HOST_GNU_TYPE == *"armel"* ]]; then
                export CFLAGS='-O2 -pipe -fno-plt -fexceptions -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security -fstack-clash-protection'
        fi

        if [[ $DEB_HOST_GNU_TYPE == *"x86_64"* ]]; then
		export CFLAGS='-march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security -fstack-clash-protection -fcf-protection'
        fi

	# Ubuntu 18.04 quirks
        # From makedeb .drone/scripts/build.sh, line 41
	if [[ "$(source /etc/os-release; echo "${VERSION_ID}")" = "18.04" ]]; then
		# Remove unsupported flags
		CFLAGS=${CFLAGS//-fstack-clash-protection/}
		CFLAGS=${CFLAGS//-fcf-protection}
		CFLAGS=$(echo "$CFLAGS" | xargs)
	fi

        cd Python-${pkgver}
        ./configure \
                --build=${DEB_BUILD_GNU_TYPE} \
                --host=${DEB_HOST_GNU_TYPE} \
                --libdir=/usr/lib \
                --prefix=/usr \
                --enable-ipv6 \
                --enable-loadable-sqlite-extensions \
                --enable-shared \
                --enable-optimizations \
                --with-computed-gotos \
                --with-dbmliborder=gdbm:ndbm \
                --with-system-expat \
                --with-lto \
                --with-system-ffi \
                --disable-test-modules \
                --without-ensurepip                  
        make
}

package() {
        cd Python-${pkgver}
        msg2 "Setting up data..."
        make DESTDIR=${pkgdir} altinstall

        # Remove unneeded library
        rm -rf ${pkgdir}/usr/lib/libpython3.so
}
